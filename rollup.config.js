import { version } from './package.json'
import buble from 'rollup-plugin-buble'

const banner = `/*!
* vue-demo-printer v${version}
* (c) ${new Date().getFullYear()} cell <cellinlab@gmail.com>
* Learn more in https://gitee.com/cellinlab/vue-dom-printer.git
* @license MIT
*/`
export default {
  input: 'src/index.js',
  output: [{
    file: 'dist/vue-dom-printer.esm.js',
    format: 'es',
    banner
  }, {
    file: 'dist/vue-dom-printer.js',
    format: 'umd',
    name: 'VuePrinter',
    banner
  }],
  plugins: [
    buble()
  ]
}