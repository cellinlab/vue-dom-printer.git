/*!
* vue-demo-printer v1.0.2
* (c) 2020 cell <cellinlab@gmail.com>
* Learn more in https://gitee.com/cellinlab/vue-dom-printer.git
* @license MIT
*/
var Print = function (dom, options) {
  if (!(this instanceof Print)) {
    return new Print(dom, options)
  }
  this.options = Object.assign({
    'noPrint': '.no-print'
  }, options);

  if ((typeof dom) === 'string') {
    this.dom = document.querySelector(dom);
  } else {
    this.dom = this.isDOM(dom) ? dom : dom.$el;
  }
  this.init();
};
Print.prototype = {
  init: function () {
    var content = this.getStyle() + this.setStyle() + this.getHtml();
    this.writeIframe(content);
  },
  // 获取样式
  getStyle: function () {
    var styleStr = '';
    var styles = document.querySelectorAll('style,link');
    for (var i = 0; i < styles.length; i++) {
      styleStr += styles[i].outerHTML;
    }
    styleStr += "<style>\n      " + (this.options.noPrint ? this.options.noPrint : '.noPrint') + " {display: none;}\n    </style>";
    return styleStr
  },
  // 根据用户需求设置页眉页脚，并修复折行和拆表等问题
  setStyle: function() {
    var noHeader = this.options.noHeader || false;
    var noFooter = this.options.noFooter || false;
    return ("<style>\n      " + ((noFooter || noFooter) ? "\n          @media print {\n            @page {\n              size: auto;\n              /* auto is the initial value */\n              margin-top: 10mm;\n              margin-left: 10mm;\n              margin-right: 10mm;\n              margin-bottom: 10mm;\n            }\n          }\n        " : '') + "\n      " + (noHeader ? "\n          @media print {\n            @page {\n              margin-top: 0mm;\n            }\n          }\n        " : '') + "\n      " + (noFooter ? "\n          @media print {\n            @page {\n              margin-bottom: 0mm;\n            }\n          }\n        " : '') + "\n    </style>")
  },
  // 获取HTMl
  getHtml: function () {
    var inputs = document.querySelectorAll('input');
    var textareas = document.querySelectorAll('textarea');
    var selects = document.querySelectorAll('select');

    // 拷贝input元素值
    for (var i$1 = 0; i$1 < inputs.length; i$1++) {
      var ref = inputs[i$1];
      var type = ref.type;
      var checked = ref.checked;
      var value = ref.value;
      if (type === 'checkbox' || type === 'radio') {
        if (checked === true) {
          inputs[i$1].setAttribute('checked', 'checked');
        } else {
          inputs[i$1].removeAttribute('checked');
        }
      } else {
        inputs[i$1].setAttribute('value', value);
      }
    }
    // 拷贝textarea值
    for (var j = 0; j < textareas.length; j++) {
      if (textareas[j].type === 'textarea') {
        textareas[j].innerHTML = textareas[j].value;
      }
    }
    // 拷贝select值
    for (var k = 0; k < selects.length; k++) {
      if (selects[k].type === 'select-one') {
        var ref$1 = selects[k];
        var children = ref$1.children;
        for (var index in children) {
          var child = children[index];
          if (child.tagName === 'OPTION') {
            if (child.selected === true) {
              child.setAttribute('selected', 'selected');
            } else {
              child[i].removeAttribute('selected');
            }
          }
        }
      }
    }
    return this.dom.outerHTML
  },
  // 将获取和拷贝的值在iframe中渲染
  writeIframe: function (content) {
    var iframe = document.createElement('iframe');
    iframe.id = 'myIframe';
    iframe.setAttribute('style', 'position:absolute;width:0;height:0;top:-10px;left:-10px;');

    var iframeDom = document.body.appendChild(iframe);
    var iframeWindow = iframeDom.contentWindow || iframeDom.contentDocument;
    var iframeDoc = iframeDom.contentDocument || iframeDom.contentWindow.document;

    iframeDoc.open();
    iframeDoc.write(content);
    iframeDoc.close();
    var self = this;
    iframe.onload = function () {
      self.toPrint(iframeWindow);
      setTimeout(function () {
        document.body.removeChild(iframe);
      }, 0);
    };
  },
  toPrint: function (window) {
    try {
      setTimeout(function () {
        window.focus();
        try {
          if (!window.document.execCommand('print', false, null)) {
            window.print();
          }
        } catch (e) {
          window.print();
        }
        window.close;
      }, 0);
    } catch (error) {
      console.error('error', error);
    }
  },
  isDOM: function (obj) {
    if (typeof HTMLElement === 'object') {
      return obj instanceof HTMLElement
    }
    return obj && typeof obj === 'object' && obj.nodeType === 1 && typeof obj.nodeName === 'string'
  }
};
var VuePrinter = {
  install: function (_Vue) {
    _Vue.prototype.$print = Print;
  }
};

export default VuePrinter;
