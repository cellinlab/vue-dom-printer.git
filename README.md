# vue-dom-printer  
Vue plugin to print DOM in browser using iframe(window.print()) not canvas.


## Install
``` shell
npm install vue-dom-printer
```

## Quick Start

``` javascript
// main.js
import Vue from 'vue'
import VuePrinter from 'vue-dom-printer'
 
Vue.use(VuePrinter)
// App.vue
this.$print(this.$refs[ref])
```
## Examples  
### Basic  
[DEMO](http://cellinlab.gitee.io/vue-dom-printer/)  
[Usage](https://gitee.com/cellinlab/vue-dom-printer/tree/master/examples/basic)

### Tips  
[Fix page-break bug](http://www.qiutianaimeili.com/html/page/2019/11/dfvmp25rvz.html)

## LICENSE
[MIT](https://gitee.com/cellinlab/vue-dom-printer/blob/master/LICENSE)  